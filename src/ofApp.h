#pragma once

#include "ofMain.h"

#define W 800
#define H 600

class ofApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();
		
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y);
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofBufferObject A1, B1, A2, B2;

		ofTexture tekstura;
		ofShader shader; 

		float A1cpu[W*H];
		float A2cpu[W*H];
		float B1cpu[W*H];
		float B2cpu[W*H];

		
		// Sound
		
		void audioIn(ofSoundBuffer & input);
	
		vector <float> left;
		vector <float> right;
		vector <float> volHistory;
		
		int 	bufferCounter;
		int 	drawCounter;
		
		float smoothedVol;
		float scaledVol;
		
		ofSoundStream soundStream;		

};
