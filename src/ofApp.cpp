#include "ofApp.h"
// #include “ofConstants.h” 


//--------------------------------------------------------------
void ofApp::setup(){
	
	shader.setupShaderFromFile(GL_COMPUTE_SHADER,"computeshader.cs");
	shader.linkProgram();

	tekstura.allocate(W,H,GL_RGBA8);
	tekstura.bindAsImage(4,GL_WRITE_ONLY);

	// initialize
	for(int x=0; x<W; x++)
	for(int y=0; y<H; y++)
	{
		int idx = x+y*W;
		A1cpu[idx] = 1.0;
		A2cpu[idx] = 1.0;

		if(rand()/float(RAND_MAX)<0.000021)
			B1cpu[idx] = 1.0; else B1cpu[idx] = 0.0;

		B2cpu[idx] = 0.0;
	}

	// allocate buffers on GPU side
	A1.allocate(W*H*sizeof(float),A1cpu,GL_STATIC_DRAW);
	A2.allocate(W*H*sizeof(float),A2cpu,GL_STATIC_DRAW);
	B1.allocate(W*H*sizeof(float),B1cpu,GL_STATIC_DRAW);
	B2.allocate(W*H*sizeof(float),B2cpu,GL_STATIC_DRAW);

	A1.bindBase(GL_SHADER_STORAGE_BUFFER,0);
	A2.bindBase(GL_SHADER_STORAGE_BUFFER,1);
	B1.bindBase(GL_SHADER_STORAGE_BUFFER,2);
	B2.bindBase(GL_SHADER_STORAGE_BUFFER,3);

	// Sound
	soundStream.printDeviceList();

	int bufferSize = 256;

	left.assign(bufferSize, 0.0);
	right.assign(bufferSize, 0.0);
	volHistory.assign(400, 0.0);
	
	bufferCounter	= 0;
	drawCounter		= 0;
	smoothedVol     = 0.0;
	scaledVol		= 0.0;

	ofSoundStreamSettings settings;	


	// if you want to set the device id to be different than the default
	// auto devices = soundStream.getDeviceList();
	// settings.device = devices[4];

	// you can also get devices for an specific api
	// auto devices = soundStream.getDevicesByApi(ofSoundDevice::Api::PULSE);
	// settings.device = devices[0];

	// or get the default device for an specific api:
	// settings.api = ofSoundDevice::Api::PULSE;

	// or by name
	auto devices = soundStream.getMatchingDevices("default");
	if(!devices.empty()){
		settings.setInDevice(devices[0]);
	}


	settings.setInListener(this);
	settings.sampleRate = 44100;
	settings.numOutputChannels = 0;
	settings.numInputChannels = 2;
	settings.bufferSize = bufferSize;
	soundStream.setup(settings);
}

//--------------------------------------------------------------
void ofApp::update(){
	
	// Show fps on window title
	std::stringstream strm;
	strm << "fps: " << ofGetFrameRate();
	ofSetWindowTitle(strm.str());

	// Bind compute shader and dispatch it
	for (int i = 0; i < 10; ++i)
	{
		// Get current time
		float time = ofGetSystemTimeMillis();
		// std::printf("time: %f\n", time);

		// SOUND
		//lets scale the vol up to a 0-1 range 
		scaledVol = ofMap(smoothedVol, 0.0, 0.17, 0.0, 1.0, true);

		//lets record the volume into an array
		volHistory.push_back( scaledVol );
		
		//if we are bigger the the size we want to record - lets drop the oldest value
		if( volHistory.size() >= 400 ){
			volHistory.erase(volHistory.begin(), volHistory.begin()+1);
		}


		// bindings (ping pong)
		static int c=1;
		c = 1-c;
		A1.bindBase(GL_SHADER_STORAGE_BUFFER, 0+c);
		A2.bindBase(GL_SHADER_STORAGE_BUFFER, 0+1-c);
		B1.bindBase(GL_SHADER_STORAGE_BUFFER, 2+c);
		B2.bindBase(GL_SHADER_STORAGE_BUFFER, 2+1-c);

		// run shader (Gray Scott)
		shader.begin();
		shader.setUniform1f("scaledVol", scaledVol);
		shader.dispatchCompute(W/20, H/20, 1);
		shader.end();

	}
	// printf("%f",scaledVol);


}

//--------------------------------------------------------------
void ofApp::audioIn(ofSoundBuffer & input){
	
	float curVol = 0.0;
	
	// samples are "interleaved"
	int numCounted = 0;	

	//lets go through each sample and calculate the root mean square which is a rough way to calculate volume	
	for (size_t i = 0; i < input.getNumFrames(); i++){
		left[i]		= input[i*2]*0.5;
		right[i]	= input[i*2+1]*0.5;

		curVol += left[i] * left[i];
		curVol += right[i] * right[i];
		numCounted+=2;
	}
	
	//this is how we get the mean of rms :) 
	curVol /= (float)numCounted;
	
	// this is how we get the root of rms :) 
	curVol = sqrt( curVol );
	
	smoothedVol *= 0.93;
	smoothedVol += 0.07 * curVol;
	
	bufferCounter++;
	
}

//--------------------------------------------------------------
void ofApp::draw(){

	tekstura.draw(0,0);

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}