#include "ofMain.h"
#include "ofApp.h"

//========================================================================
int main( ){

	// ofSetupOpenGL(1024,768, OF_WINDOW);			// <-------- setup the GL context

	ofGLWindowSettings settings;
	settings.setSize(W,H); // this is needed if you work in linux
	// settings.width = W;
	// settings.height = H;
	ofCreateWindow(settings);
	ofRunApp(new ofApp());

	// this kicks off the running of my app
	// can be OF_WINDOW or OF_FULLSCREEN
	// pass in width and height too:

	ofRunApp( new ofApp());

}
