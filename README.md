# Sound reactive Gray-Scott model

## Description
OpenFrameworks compute shader implementation of the Gray-Scott model of reaction diffusion that reacts to the sound input of the microphone. I basically reproduce the [awesome work](http://maciejmatyka.blogspot.com/2022/01/compute-shaders-in-open-frameworks.html) done by Maciej Matyka and add it a simple sound reaction.

https://www.youtube.com/watch?v=tLpm_UNEbcA

![](./sample.gif)

